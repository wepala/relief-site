var map;
var markers = [];
var locations = [];
var locationsURL = 'https://script.googleusercontent.com/macros/echo?user_content_key=c64u7btppDxkEoxz2-26lE1wwHMJcCYVCK7h5YM7PM_eVQpzoI2iochYUHyHTmn5LzgbNReRt8cWpOmwdEkYcqTLLv_w3j-em5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnNhVLK2F6FJpqPc8oZQWaB2VeI_VQJGlTtU5OVxqSvDI2-DFaltSCqU25WLwoisoW0WY2nZ7HPw5&lib=MvUvMpFXH5QOTYEE7lwqKy9qbKeHUbra5';

function getUpperCaseLocationInfo(locationElement, className) {
  if (locationElement.getElementsByClassName(className).length > 0) {
    return locationElement.getElementsByClassName(className)[0].textContent.trim().toUpperCase();
  }
  return '';
}

function searchCentres() {
  var input = document.getElementById('relief-search');
  var searchTerm = input.value.toUpperCase();
  var locationCards = document.getElementsByClassName('card-container');

  for(let i = 0; i < locationCards.length; i++) {
    let title = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-title');
    let description = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-description');
    let email = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-email');
    let website = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-website');
    let contact1 = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-contact-1');
    let phone1 = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-phone-1');
    let contact2 = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-contact-2') ;
    let phone2 = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-phone-2');
    let address = getUpperCaseLocationInfo(locationCards[i].getElementsByClassName('location-card')[0], 'location-address');

    if (title.indexOf(searchTerm) > -1 || description.indexOf(searchTerm) > -1 ||
    email.indexOf(searchTerm) > -1 || website.indexOf(searchTerm) > -1 ||
    contact1.indexOf(searchTerm) > -1 || phone1.indexOf(searchTerm) > -1 ||
    contact2.indexOf(searchTerm) > -1 || phone2.indexOf(searchTerm) > -1 ||
    address.indexOf(searchTerm) > -1) {
      locationCards[i].style.display = '';
    } else {
      locationCards[i].style.display = 'none';
    }
  }
}

function addMarker(map,latitude,longitude,title,description,address1,address2,town,phone1,phone2,email,website,contact1,contact2) {
  var point = {lat: latitude, lng: longitude}
  var marker = new google.maps.Marker({position: point, map: map, title: title});
  var contentString = '<div class="col-md-12">\n' +
    '              <div class="card">\n' +
    '                  <div class="card-body location-card">\n' +
    '                      <h4 class="location-title">\n' +
    title+
    '                      </h4>\n' +
    '                      <div class="row">\n' +
    '                        <div class="col-md-6 location-data">\n' +
    '                            <p class="location-description">\n' +
    (description != undefined ? description : '')+
    '                            </p>\n' +
    '                            <p>\n' +
    '                                Email: <strong class="location-email">'+(email != undefined ? email : '')+'</strong>\n' +
    '                                <br/>\n' +
    '                                Website: <strong class="location-website">'+(website != undefined ? website : '')+'</strong>\n' +
    '                                <br/>\n' +
    '\n' +
    '                                <a href="{{.facebook}}" target="_blank">Facebook</a>\n' +
    '\n' +
    '                            </p>\n' +
    '                        </div>\n' +
    '                        <div class="col-md-6 location-data">\n' +
    '                          <p>\n' +
    '                              Contact: <strong class="location-contact-1">'+(contact1 != undefined ? contact1 : '')+'</strong>\n' +
    '                              <br/>\n' +
    '                              Phone: <strong class="location-phone-1">'+(phone1 != undefined ? phone1 : '')+'</strong>\n' +
    '                          </p>\n' +
    '                          <p>\n' +

    '                              Contact: <strong class="location-contact-2">'+(contact2 != undefined ? contact2 : '')+'</strong>\n' +
    '                              <br/>\n' +

    '\n' +

    '                              Phone: <strong class="location-phone-2">'+(phone2 != undefined ? phone2 : '')+'</strong>\n' +

    '\n' +
    '                          </p>\n' +
    '                          <p>\n' +

    '                              Location: <strong class="location-address">'+(address1 != undefined ? address1 : '')+' '+(address2 != undefined ? address2 : '')+', '+(town != undefined ? town : '')+'</strong>\n' +

    '\n' +
    '                          </p>\n' +
    '\n' +
    '                        </div>\n' +
    '\n' +
    '                      </div>\n' +
    '\n' +
    '                  </div>\n' +
    '              </div>\n' +
    '              <br/>\n' +
    '            </div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  markers.push(marker);
}

function addListItem(latitude,longitude,title,description,address1,address2,town,phone1,phone2,email,website,contact1,contact2) {
  if (title == undefined || title == "") {
      return ;
  }
  var contentString = '<div class="col-md-12 card-container">\n' +
                        '<div class="card">\n' +
      '                     <div class="card-body location-card">\n' +
      '                       <h4 class="location-title">'+title+'</h4>\n' +
      '                      <div class="row">\n' +
      '                        <div class="col-md-6 location-data">\n' +
      '                            <p class="location-description">\n' +
      (description != undefined ? description : '')+
      '                            </p>\n' +
      '                            <p>\n' +
      '                                Email: <strong class="location-email">'+(email != undefined ? email : '')+'</strong>\n' +
      '                                <br/>\n' +
      '                                Website: <strong class="location-website">'+(website != undefined ? website : '')+'</strong>\n' +
      '                                <br/>\n' +
      '\n' +
      '                                <a href="{{.facebook}}" target="_blank">Facebook</a>\n' +
      '\n' +
      '                            </p>\n' +
      '                        </div>\n' +
      '                        <div class="col-md-6 location-data">\n' +
      '                          <p>\n' +
      '                              Contact: <strong class="location-contact-1">'+(contact1 != undefined ? contact1 : '')+'</strong>\n' +
      '                              <br/>\n' +
      '                              Phone: <strong class="location-phone-1">'+(phone1 != undefined ? phone1 : '')+'</strong>\n' +
      '                          </p>\n' +
      '                          <p>\n' +
      '                              Contact: <strong class="location-contact-2">'+(contact2 != undefined ? contact2 : '')+'</strong>\n' +
      '                              <br/>\n' +
      '\n' +
      '                              Phone: <strong class="location-phone-2">'+(phone2 != undefined ? phone2 : '')+'</strong>\n' +
      '\n' +
      '                          </p>\n' +
      '                          <p>\n' +
      '                              Location: <strong class="location-address">'+(address1 != undefined ? address1 : '')+(address2 != undefined ?' '+ address2 : '')+(town != undefined ? ', '+town : '')+'</strong>\n' +
      '\n' +
      '                          </p>\n' +(latitude != undefined ? '<p> <a href="https://www.google.com/maps/?q='+latitude+','+longitude+'" target="_blank">View on Google Maps</a> </p>' :'')
      '\n' +
      '                        </div>\n' +
      '\n' +
      '                      </div>\n' +
      '\n' +
      '                  </div>\n' +
      '              </div>\n' +
      '            </div>\n';

  var listHolder = document.getElementById("listHolder");
  listHolder.innerHTML += contentString;
}

function sortAlphabetically(a, b) {
  if (a.dropOffLocationName < b.dropOffLocationName)
    return -1;
  if (a.dropOffLocationName > b.dropOffLocationName)
    return 1;
  return 0;
}

$(document).ready(function() {
  var request = $.get(locationsURL, function() {
    console.log('Querying Relief API');
  })
  .done(function(data) {
    locations = data.response.data;
    // Sort list
    locations.sort(sortAlphabetically)
    // Update the list
    for (var i=0; i<locations.length; i++) {
      addListItem(locations[i].latitude,locations[i].longitude,locations[i].dropOffLocationName,locations[i].description,locations[i].address1,locations[i].address2,locations[i].town,locations[i].phone1,locations[i].phone2,locations[i].email,locations[i].website,locations[i].contactName1,locations[i].contactName2);
    }
    // Reload map markers
    reloadMarkers();
  })
  .fail(function(error) {
    console.error(error);
  })
});

// Initialize and add the map
function initMap() {
  // The location of trinidad
  var trinidad = {lat: 10.387634, lng: -61.266413};
  // The map, centered at trinidad
  map = new google.maps.Map(document.getElementById('map'), {zoom: 9, center: trinidad});
  setMarkers(locations);
}

function setMarkers(locations) {
  for (var i = 0; i < locations.length; i++) {
    if (locations[i].latitude != undefined && locations[i].latitude != "") {
      addMarker(map, locations[i].latitude,locations[i].longitude,locations[i].dropOffLocationName,locations[i].description,locations[i].address1,locations[i].address2,locations[i].town,locations[i].phone1,locations[i].phone2,locations[i].email,locations[i].website,locations[i].contact1,locations[i].contact2);
    }
  }
}

function reloadMarkers() {
  // Loop through markers and set map to null for each
  for (var i=0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  // Reset the markers array
  markers = [];
  // Call set markers to re-add markers
  setMarkers(locations);
}
